package com.zero2oneit.mall.member.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zero2oneit.mall.common.bean.member.PrizeWinMapping;
import com.zero2oneit.mall.common.query.member.PrizeWinMappingQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-25
 */
public interface PrizeWinMappingService extends IService<PrizeWinMapping> {

    BoostrapDataGrid pageList(PrizeWinMappingQueryObject qo);
}

