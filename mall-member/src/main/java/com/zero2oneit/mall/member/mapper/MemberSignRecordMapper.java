package com.zero2oneit.mall.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zero2oneit.mall.common.bean.member.MemberSignRecord;
import com.zero2oneit.mall.common.query.member.MemberSignRecordQueryObject;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author Tg
 * @create 2021-05-13
 * @description
 */
@Mapper
public interface MemberSignRecordMapper extends BaseMapper<MemberSignRecord> {

    int selectTotal(MemberSignRecordQueryObject qo);

    List<Map<String, Object>> selectAll(MemberSignRecordQueryObject qo);

    int selectTotals(MemberSignRecordQueryObject qo);

    List<Map<String, Object>> selectAlls(MemberSignRecordQueryObject qo);

}
