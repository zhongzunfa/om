package com.zero2oneit.mall.member.controller;

import com.zero2oneit.mall.common.bean.member.PrizeWin;
import com.zero2oneit.mall.common.bean.member.PrizeWinMapping;
import com.zero2oneit.mall.common.query.member.PrizeWinMappingQueryObject;
import com.zero2oneit.mall.common.query.member.PrizeWinQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.zero2oneit.mall.member.service.PrizeWinMappingService;

import java.util.Arrays;
import java.util.Date;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-25
 */
@RestController
@RequestMapping("/admin/member/prizeWinMapping")
public class PrizeWinMappingController {

    @Autowired
    private PrizeWinMappingService prizeWinMappingService;

    /**
     * 查询中奖记录列表信息
     * @param qo
     * @return
     */
    @PostMapping("/list")
    public BoostrapDataGrid list(@RequestBody PrizeWinMappingQueryObject qo){
        return prizeWinMappingService.pageList(qo);
    }

    /**
     * 添加或编辑中奖会员信息
     * @param prizeWinMapping
     * @return
     */
    @PostMapping("/addOrEdit")
    public R addOrEdit(@RequestBody PrizeWinMapping prizeWinMapping){
        prizeWinMappingService.saveOrUpdate(prizeWinMapping);
        return R.ok();
    }

    /**
     * 更改中奖记录状态
     * @param id
     * @param status
     * @return
     */
    @PostMapping("/status")
    public R status(@RequestParam("id") String id, @RequestParam("status") Integer status){

        PrizeWinMapping prizeWinMapping = new PrizeWinMapping();
        prizeWinMapping.setId(Long.valueOf(id));
        if (status==1){
            prizeWinMapping.setStatusId(0);
        }else if (status==0){
            prizeWinMapping.setStatusId(1);
        }
        return prizeWinMappingService.updateById(prizeWinMapping) == true ? R.ok("更改状态成功"):R.fail("更改状态失败");
    }

    /**
     * 根据ID删除中奖会员信息
     * @param ids
     * @return
     */
    @PostMapping("/delByIds")
    public R delByIds(@RequestParam("ids") String ids){
        return prizeWinMappingService.removeByIds(Arrays.asList(ids.split(","))) == true ? R.ok("删除成功") : R.fail("删除失败");
    }


}
