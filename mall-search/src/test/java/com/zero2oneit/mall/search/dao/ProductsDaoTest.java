package com.zero2oneit.mall.search.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021/5/30
 */
@SpringBootTest
public class ProductsDaoTest {

    //注入ElasticsearchRestTemplate
    @Autowired
    private ElasticsearchRestTemplate template;

    @Autowired
    private ProductsDao productsDao;

    //创建索引并增加映射配置
    @Test
    public void createIndex(){
        //创建索引，系统初始化会自动创建索引
        System.out.println("创建索引");
    }

}